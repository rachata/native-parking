package com.example.parking;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FindFavoriteActivity extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_favorite);


        mRecyclerView = (RecyclerView) findViewById(R.id.findAllList);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));



        final DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

        final String keyUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final DatabaseReference dataPlace = mRootRef.child("data_user/"+keyUser+"/favorite");

        dataPlace.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                final List<String > list = new ArrayList<>();

                for(DataSnapshot mss : dataSnapshot.getChildren()){

                    list.add(mss.getKey());
                    Log.i("wefewf" , mss.getKey());
                }


                    mAdapter = new AdapterFindFavorite(list, getApplicationContext());
                    mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
