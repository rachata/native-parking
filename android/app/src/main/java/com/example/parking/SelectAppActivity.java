package com.example.parking;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.parking.Helper.LocaleHelper;

import java.util.Locale;

import io.paperdb.Paper;


public class SelectAppActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Paper.init(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_app);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String language = Paper.book().read("language");
        String getLang = Locale.getDefault().getLanguage();

        if(language == null) {
            Paper.book().write("language", "en");
        }else{
            if(language.equals(getLang)){
                updateView((String)Paper.book().read("language"));
            }else{
                updateView((String)Paper.book().read("language"));
            }
        }

        updateView((String)Paper.book().read("language"));

        ImageView imgDDpark = (ImageView) findViewById(R.id.ddparking);
        ImageView ddproking = (ImageView) findViewById(R.id.ddproking);
        ImageView dtracker = (ImageView) findViewById(R.id.dtracker);

        TextView txtName = (TextView) findViewById(R.id.txtName);

        txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "http://www.detectinno.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);


            }
        });

        imgDDpark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("sdadasda","wsewewewewew");
                startActivity(new Intent(getApplicationContext(), ControlPanelActivity.class));
            }
        });

        ddproking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.detectinno.com/decons";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        dtracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.detectinno.com/dtracker";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    private  void  updateView(String lang){
        Context context = LocaleHelper.setLocale(this,lang);
        context.getResources();
//        Resources resources = context.getResources();
//        textView.setText(resources.getString(R.string.hello));

        Configuration config = new Configuration();
        config.locale = new Locale(lang);
        getResources().updateConfiguration(config, null);

    }
}
