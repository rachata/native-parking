package com.example.parking;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;


@IgnoreExtraProperties
public class Place {

    @PropertyName("ATotalZone")  private int ATotalZone;
    @PropertyName("imageZone") private String imageZone;
    @PropertyName("lat") private double lat;
    @PropertyName("lng") private double lng;
    @PropertyName("nameEN") private String nameEN;
    @PropertyName("nameTH") private String nameTH;
    @PropertyName("openingTime") private String openingTime;
    @PropertyName("priceRange") private double priceRange;

    private int lot;


    public int getLot() {
        return lot;
    }

    public void setLot(int lot) {
        this.lot = lot;
    }

    public Place(){

    }

    public Place(int ATotalZone, String imageZone, double lat, double lng, String nameEN, String nameTH, String openingTime, double priceRange) {
        this.ATotalZone = ATotalZone;
        this.imageZone = imageZone;
        this.lat = lat;
        this.lng = lng;
        this.nameEN = nameEN;
        this.nameTH = nameTH;
        this.openingTime = openingTime;
        this.priceRange = priceRange;
    }

    public int getATotalZone() {
        return ATotalZone;
    }

    public void setATotalZone(int ATotalZone) {
        this.ATotalZone = ATotalZone;
    }

    public String getImageZone() {
        return imageZone;
    }

    public void setImageZone(String imageZone) {
        this.imageZone = imageZone;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public double getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(double priceRange) {
        this.priceRange = priceRange;
    }
}
