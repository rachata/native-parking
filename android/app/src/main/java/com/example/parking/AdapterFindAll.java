package com.example.parking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AdapterFindAll extends RecyclerView.Adapter<AdapterFindAll.ViewHolder> {



    private List<String> data;
    private List<String> listF;
    private Context context;


    public AdapterFindAll(List<String> data , List<String> listF , Context context ){

        this.data = data;
        this.listF = listF;
        this.context = context;

    }


    @NonNull
    @Override
    public AdapterFindAll.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_find, viewGroup, false);

        return new AdapterFindAll.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFindAll.ViewHolder viewHolder, int i) {

        String name = data.get(i);


        Log.i("wefwef" , "wefewf");
        if(listF != null){
            if(listF.contains(name)){


                viewHolder.imageFa.setTag(R.drawable.star_y);
                viewHolder.imageFa.setImageDrawable( context. getResources().getDrawable(R.drawable.star_y));
                viewHolder.name.setText(name);

            }else{
                viewHolder.imageFa.setTag(R.drawable.star_b);
                viewHolder.imageFa.setImageDrawable( context. getResources().getDrawable(R.drawable.star_b));
                viewHolder.name.setText(name);
            }
        }else{
            viewHolder.imageFa.setTag(R.drawable.star_b);
            viewHolder.imageFa.setImageDrawable( context. getResources().getDrawable(R.drawable.star_b));
            viewHolder.name.setText(name);
        }





    }

    public void updateList(List<String> newList){
        data = new ArrayList<>();
        data.addAll(newList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name;
    public ImageView imageFa;


        public ViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.txtName);
            imageFa = (ImageView) view.findViewById(R.id.imageFa);


            imageFa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                   int tag  = (int) imageFa.getTag();

                   int id  =  R.drawable.star_b;



                    Log.i("ewknfbj" , id + " " + tag);
                    if(tag == id){
                        String keyPlace = data.get(getLayoutPosition());

                        DatabaseReference database = FirebaseDatabase.getInstance().getReference();


                        String keyUser = FirebaseAuth.getInstance().getCurrentUser().getUid();

                        String key = database.child("data_user/"+keyUser+"/favorite").push().getKey();



                        database.child("data_user/"+keyUser+"/favorite").child(keyPlace).setValue(keyPlace);



                    }else{

                        String keyUser = FirebaseAuth.getInstance().getCurrentUser().getUid();

                        String keyPlace = data.get(getLayoutPosition());

                        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                        mRootRef.child("data_user/"+keyUser+"/favorite/"+keyPlace).removeValue();
                    }






                }
            });




            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(context , PreviewActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("data" , data.get(getLayoutPosition()));
            context.startActivity(intent);
        }
    }
}
