package com.example.parking;


import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toolbar;

import io.paperdb.Paper;

public  class ViewMapsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_maps);
        Paper.init(this);
        GoogleMapsFragment googleMapsFragment = new GoogleMapsFragment();
        replaceFragmentManager(googleMapsFragment);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        getMenuInflater().inflate(R.menu.search_bar,menu);
        return  true;
    }


    private void replaceFragmentManager(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.viewsMaps, fragment);
        transaction.addToBackStack("test");
        transaction.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.i("asassa", String.valueOf(getSupportFragmentManager().getBackStackEntryCount()));


            if(item.getItemId() == 16908332 ){
                if(getSupportFragmentManager().getBackStackEntryCount() >= 1){
                    startActivity(new Intent(ViewMapsActivity.this, ControlPanelActivity.class));
                    getSupportFragmentManager().popBackStack();
                    finish();
                }else{
                    getSupportFragmentManager().popBackStack();
                }
            }



        return super.onOptionsItemSelected(item);
    }
}
