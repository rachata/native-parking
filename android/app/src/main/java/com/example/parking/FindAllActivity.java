package com.example.parking;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FindAllActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    private List<String> data;
    private ArrayList<String> listF;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_all);


        data = new ArrayList<>();
        listF = new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.findAllList);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        final DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference dataPlace = mRootRef.child("/");

        dataPlace.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    final List<String > list = new ArrayList<>();
                    data.removeAll(data);
                    for(DataSnapshot mss  : dataSnapshot.getChildren()){

                        if(!(mss.getKey().equals("data") || mss.getKey().equals("data_user"))){
                            Log.i("DataSnapshot" , mss.getKey()+"");
                            data.add(mss.getKey());
                        }

                    }

                    if(data.size() >= 1){

                        String keyUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        DatabaseReference mNewsRef = mRootRef.child("data_user/"+keyUser+"/favorite");

                        mNewsRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {



                                listF.removeAll(listF);
                                for(DataSnapshot messageSnapshot : dataSnapshot.getChildren()){

                                    listF.add(messageSnapshot.getKey());

                                }


                                if(listF.size() >= 1){
                                    mAdapter = new AdapterFindAll(data, listF ,  getApplicationContext());
                                    mRecyclerView.setAdapter(mAdapter);

                                    Log.i("favorite"  , "1");
                                }else{
                                    Log.i("favorite"  , "null");

                                    mAdapter = new AdapterFindAll(data, listF ,  getApplicationContext());
                                    mRecyclerView.setAdapter(mAdapter);
                                }

                            }



                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });



                    }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar_search , menu);
        MenuItem menuItem = menu.findItem(R.id.searchPlace);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {


        String userSearch = s.toLowerCase();
        List<String> newList = new  ArrayList<>();

        for(String name : data){
            if(name.toLowerCase().contains(userSearch)){
                newList.add(name);
            }
        }

        mAdapter = new AdapterFindAll(newList, listF ,  getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);

        return false;
    }
}
