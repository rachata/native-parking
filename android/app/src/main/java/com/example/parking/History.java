package com.example.parking;

public class History {

    private String place;
    private String zone;
    private String lot;
    private String startDate;
    private String endDate;
    private int priceStatus;
    private double price;
    private String priceShow;


    public History(){}
    public History(String place, String zone, String lot, String startDate, String endDate, int priceStatus, double price, String priceShow) {
        this.place = place;
        this.zone = zone;
        this.lot = lot;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceStatus = priceStatus;
        this.price = price;
        this.priceShow = priceShow;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getPriceStatus() {
        return priceStatus;
    }

    public void setPriceStatus(int priceStatus) {
        this.priceStatus = priceStatus;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPriceShow() {
        return priceShow;
    }

    public void setPriceShow(String priceShow) {
        this.priceShow = priceShow;
    }
}
