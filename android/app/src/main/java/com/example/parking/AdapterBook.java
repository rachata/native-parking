package com.example.parking;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AdapterBook  extends RecyclerView.Adapter<AdapterBook.ViewHolder>{


    private List<History> data;
    private Context context;


    public AdapterBook(List<History> data, Context context ){

        this.data = data;
        this.context = context;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_booking, viewGroup, false);

        return new AdapterBook.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {


        History history = data.get(i);


        viewHolder.txtPlace.setText(history.getPlace());
        viewHolder.txtZone.setText(history.getZone());
        viewHolder.txtLot.setText(history.getLot());
        viewHolder.txtDate.setText(history.getStartDate()+" - " + history.getEndDate());


        viewHolder.txtPlace.setTextColor(Color.parseColor("#FFFFFF"));
        viewHolder.txtZone.setTextColor(Color.parseColor("#FFFFFF"));
        viewHolder.txtLot.setTextColor(Color.parseColor("#FFFFFF"));
        viewHolder.txtDate.setTextColor(Color.parseColor("#FFFFFF"));


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView txtPlace;
        public TextView txtZone;
        public TextView txtLot;
        public TextView txtDate;


        public ViewHolder(View view) {
            super(view);

            txtPlace = (TextView) view.findViewById(R.id.txtPlace);
            txtZone = (TextView) view.findViewById(R.id.txtZone);
            txtLot = (TextView) view.findViewById(R.id.txtLot);

            txtDate = (TextView) view.findViewById(R.id.txtDate);


            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
