package com.example.parking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

public class adapterZone extends RecyclerView.Adapter<adapterZone.ViewHolder> {

    private List<Map<String, Zone>> data;
    private Context context;

    public adapterZone(List<Map<String, Zone>> data, Context context) {

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public adapterZone.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_detail_parking, viewGroup, false);

        return new adapterZone.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterZone.ViewHolder viewHolder, int i) {


        Map<String, Zone> map = data.get(i);

        Zone zone = map.entrySet().iterator().next().getValue();
//        News news = data.get(i);
//
//
        viewHolder.total.setText(zone.getAAvailable() + "/" + zone.getATotalLot());
        viewHolder.total.setTextColor(Color.parseColor("#FFFFFF"));
        viewHolder.nameZone.setText(zone.getName());
        viewHolder.nameZone.setTextColor(Color.parseColor("#FFFFFF"));

        Log.i("zone.getAAvailable()", zone.getAAvailable() + "");
        Log.i("zone.getATotalLot()", zone.getATotalLot() + "");


        float a = zone.getAAvailable();
        float b = zone.getATotalLot();

        float progress = a / b * 100;

        Log.i("process", progress + "");
        viewHolder.progressBar.setProgress((int) progress);
    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView total;
        public TextView nameZone;
        public ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);

            total = (TextView) view.findViewById(R.id.total);
            nameZone = (TextView) view.findViewById(R.id.nameZone);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            Map<String, Zone> map = data.get(getLayoutPosition());

            Zone zone = map.entrySet().iterator().next().getValue();
            
            Log.i("place", zone.getPlace());
            Log.i("zone", zone.getKey());
            Log.i("zone", zone.getPrice()+"");

            Intent intent = new Intent(context, TestCanvasActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("place", zone.getPlace());
            intent.putExtra("zone", zone.getKey());
            intent.putExtra("price", zone.getPrice());

            context.startActivity(intent);
        }
    }
}
