package com.example.parking;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;


/**
 * A simple {@link Fragment} subclass.
 */
public class GoogleMapsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private MapView mMapView;
    private View mView;

    private int lot;
    private List<Map<String, Place>> listMapClick;


    public GoogleMapsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle(R.string.parking);
        mView = inflater.inflate(R.layout.fragment_google_maps, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = (MapView) mView.findViewById(R.id.map2);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        final DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

        final DatabaseReference dataMap = mRootRef.child("/");


        final List<Map<String, Place>> listMap = new ArrayList<>();
        dataMap.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot mss : dataSnapshot.getChildren()) {





                    if (!(mss.getKey().equals("data") || mss.getKey().equals("data_user"))) {

                        final String key = mss.getKey();

                        final Place place = mss.getValue(Place.class);




                        DatabaseReference dataPlace = mRootRef.child(mss.getKey() + "/");

                        dataPlace.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                                for (DataSnapshot mss : dataSnapshot.getChildren()) {



                                    if (!((mss.getKey().equals("nameTH")) || (mss.getKey().equals("ATotalZone")) ||
                                            (mss.getKey().equals("imageZone")) || (mss.getKey().equals("lat")) ||
                                            (mss.getKey().equals("lng")) || (mss.getKey().equals("long")) ||
                                            (mss.getKey().equals("nameEN")) || (mss.getKey().equals("openingTime")) ||
                                            (mss.getKey().equals("priceRange")))) {


                                        Zone zone = mss.getValue(Zone.class);

                                        lot += zone.getAAvailable();

                                    }
                                }

                                place.setLot(lot);

                                lot = 0;
                                Log.i("place lot", place.getLot() + "");
                                Log.i("place lot", place.getNameEN()   + "");

                                Map<String, Place> map = new HashMap<>();

                                Log.i("place name", key);
                                map.put(key,place);

                                listMap.add(map);

                                listMapClick = listMap;
                                addMarker(listMap);


                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });




                    }
                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Double lat = marker.getPosition().latitude;
        Double lng = marker.getPosition().longitude;


        Double price = 0.0;
        String key = marker.getTitle();
        String name = null;
//        Log.i("onMarkerClick ", key + " " + lat + "/" + lng);


        for (Map<String, Place> markerList : listMapClick) {

            Place place = markerList.entrySet().iterator().next().getValue();
            String keyPlace = markerList.entrySet().iterator().next().getKey();


//            String language = Paper.book().read("language");
//            if (language.equals("th")){
//                key = place.getNameEN();
//                name = marker.getTitle();
//            }else{
//                key = marker.getTitle();
//                name = place.getNameEN();
//            }



//            Log.i("keypalce" , keyPlace);

            price = place.getPriceRange();

//            if (keyPlace.equals(key)) {
//                Log.i("place list ", place.getNameEN());
//                Log.i("place marker ", key);
//
//            }

        }


//        Bundle bundle = new Bundle();
//        bundle.putString("place", key);
//        bundle.putDouble("lat", lat);
//        bundle.putDouble("lng", lng);
//        bundle.putDouble("price", price);
//
//        ParkingDetailFragment parkingDetailFragment = new ParkingDetailFragment();
//        parkingDetailFragment.setArguments(bundle);
//
//        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.viewsMaps, parkingDetailFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();


                Intent intent  =  new Intent(getContext(), ParkingDetailActivity.class);
                intent.putExtra("place" , key);
                intent.putExtra("name" , name);
                intent.putExtra("price" , price);
                intent.putExtra("lat" , lat);
                intent.putExtra("lng" , lng);
                startActivity(intent);



        return false;

    }

    private void addMarker(List<Map<String, Place>> listMap) {


        if (listMap.size() >= 1) {

            for (Map<String, Place> mapPlace : listMap) {

                String language = Paper.book().read("language");





                Place place = mapPlace.entrySet().iterator().next().getValue();
                String key = mapPlace.entrySet().iterator().next().getKey();
//                Log.i("Kry -->", String.valueOf(key.equals("nameTh")));
//                Log.i("Kry -->", String.valueOf(place.getNameTH()));
                LatLng latLng = new LatLng(place.getLat(), place.getLng());
                String name;

//                if(language.equals("th")){
//                    Log.i("lang",language);
//                     name = place.getNameTH();
//                }else{
//                    Log.i("lang",language);
//                     name = place.getNameEN();
//                }



                Log.i("marker lot" , place.getLot()+"");
                int id = R.drawable.m0;
                if(place.getLot() == 0){
                    id = R.drawable.m0;
                }else  if(place.getLot() == 1){
                    id = R.drawable.m1;
                }else  if(place.getLot() == 2){
                    id = R.drawable.m2;
                }else  if(place.getLot() == 3){
                    id = R.drawable.m3;
                }else  if(place.getLot() == 4){
                    id = R.drawable.m4;
                }else  if(place.getLot() == 5){
                    id = R.drawable.m5;
                }else  if(place.getLot() == 6){
                    id = R.drawable.m6;
                }else  if(place.getLot() == 7){
                    id = R.drawable.m7;
                }else  if(place.getLot() == 8){
                    id = R.drawable.m8;
                }else  if(place.getLot() == 9){
                    id = R.drawable.m9;
                }else  if(place.getLot() >= 10){
                    id = R.drawable.m10;
                }


                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(id);
                Bitmap b = bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, 100, 100, false);




                mMap.addMarker(new MarkerOptions()
                        .position(latLng).title(key)
                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                mMap.setOnMarkerClickListener(this);

            }
        }

        LatLng thai = new LatLng(13.7649, 100.5383);
        CameraPosition.Builder cameraPosition = new CameraPosition.Builder();
        cameraPosition.target(thai);
        cameraPosition.zoom(6);
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition.build()));
    }


}
