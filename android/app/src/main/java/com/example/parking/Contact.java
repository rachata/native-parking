package com.example.parking;

public class Contact {

    private String name;
    private String detail;
    private String topic;
    private String tel;
    private String email;
    private String uid;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Contact(String name, String detail, String topic, String tel, String email, String uid) {
        this.name = name;
        this.detail = detail;
        this.topic = topic;
        this.tel = tel;
        this.email = email;
        this.uid = uid;
    }
}
