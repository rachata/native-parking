package com.example.parking;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_news, container, false);


        mRecyclerView = (RecyclerView) v.findViewById(R.id.newsList);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

        DatabaseReference mNewsRef = mRootRef.child("data/news");

        mNewsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<News> list = new ArrayList<>();

                for(DataSnapshot messageSnapshot : dataSnapshot.getChildren()){

                    News news = messageSnapshot.getValue(News.class);

                    list.add(news);
                }


                if(list.size() >= 1){
                    Log.i("list.size()" , list.size()+"");
                    mAdapter = new AdapterNews(list, getContext());
                    mRecyclerView.setAdapter(mAdapter);
                }

            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        getActivity().setTitle(R.string.news);
        return v;
    }

}
