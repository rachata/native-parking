package com.example.parking;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;


public class ParkingDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;


    // TODO: Rename and change types of parameters
    private String place;
    private double lat;
    private double lng;

    private  double price;

    View view;

    public ParkingDetailFragment() {
        // Required empty public constructor
    }

    public static ParkingDetailFragment newInstance(String param1, String param2) {
        ParkingDetailFragment fragment = new ParkingDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            place = getArguments().getString("place");
            lat = getArguments().getDouble("lat");
            lng = getArguments().getDouble("lng");
            price = getArguments().getDouble("price");
            Log.i("test",place);
        }

        getActivity().setTitle(place);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_parking_detail, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.zoneList);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        Button btnGoogleMap = (Button) v.findViewById(R.id.btnGoogleMap);
        btnGoogleMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://maps.google.com/maps?f=d&daddr="+lat+","+lng+"&dirflg=d&layer=t";
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });
        Log.i("onCreateView" , place);

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();


        final TextView txt_number = (TextView) v.findViewById(R.id.txt_number);
        final DatabaseReference dataPlace = mRootRef.child(place+"/");



        dataPlace.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<Map<String , Zone>> list = new ArrayList<>();
                for(DataSnapshot mss : dataSnapshot.getChildren()){


                    if(!(mss.getKey().equals("ATotalZone") || mss.getKey().equals("imageZone") || mss.getKey().equals("lat") || mss.getKey().equals("lng") ||
                            mss.getKey().equals("long") || mss.getKey().equals("nameEN") || mss.getKey().equals("nameTH") || mss.getKey().equals("openingTime")||
                            mss.getKey().equals("priceRange"))){

                        Log.i("mss" , mss.getKey());



                        Zone zone = mss.getValue(Zone.class);

                        zone.setPlace(place);
                        zone.setKey(mss.getKey());
                        zone.setPrice(price);
                        Log.i("zone " , zone.getAAvailable()+"");
                        Log.i("zone " , zone.getATotalLot()+"");

                        Map<String , Zone> map = new HashMap<>();
                        map.put(mss.getKey() , zone);
                        list.add(map);


                    }
                }



                if(list.size() >= 1){

                    txt_number.setText(list.size()+"");


                    mAdapter = new adapterZone(list, getContext());
                    mRecyclerView.setAdapter(mAdapter);


                }else{
                    txt_number.setText("Null");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return v;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        MenuItem item=menu.findItem(R.id.search);
//        item.setVisible(false);
//        super.onCreateOptionsMenu(menu, inflater);
//    }

//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//
//        inflater.inflate(R.menu.search_bar, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.search) {
//
//            Log.i("RRRR" , "search");
//
//            return true;
//
//        }else if(id == R.id.search2){
//            Log.i("RRRR" , "search2");
//            return true;
//        }
//
//
//
//        return super.onOptionsItemSelected(item);
//    }
}
