package com.example.parking;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import com.google.zxing.Result;
public class ScanBarcodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {



    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_barcode);

        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();



    }


    @Override
    public void handleResult(Result rawResult) {

        mScannerView.stopCamera();
        Intent itent = new Intent();
        itent.putExtra("data", rawResult.getText());
        setResult(RESULT_OK, itent);
        finish();
    }


}
