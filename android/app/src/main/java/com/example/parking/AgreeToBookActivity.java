package com.example.parking;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.paperdb.Paper;

public class AgreeToBookActivity extends AppCompatActivity {


    private TextView txtLot;
    private  TextView txtStart;
    private  TextView txtEnd ;
    private TextView txtPrice;
    private TextView txtPlace;
    private TextView txtZone;
    private  String name;

    private String datetimeStart;
    private String datetimeEnd;
    private SwitchDateTimeDialogFragment dateTimeFragmentStart;

    private SwitchDateTimeDialogFragment dateTimeFragmentEnd;
    private static final String TAG_DATETIME_FRAGMENT_STRAT = "TAG_DATETIME_FRAGMENT_START";
    private static final String TAG_DATETIME_FRAGMENT_END = "TAG_DATETIME_FRAGMENT_END";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agree_to_book);



        final String place = getIntent().getStringExtra("place");
        final String zone = getIntent().getStringExtra("zone");
        final String lot = getIntent().getStringExtra("lot");
        final boolean statusBook = getIntent().getBooleanExtra("status" , false);
        final double price = getIntent().getDoubleExtra("price" , -1);


//        getSupportActionBar().setTitle(place);
        DatabaseReference mRootRefer = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mTitleRef = mRootRefer.child(place + "/nameTH");

        mTitleRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                name = (String) dataSnapshot.getValue();
                Log.i("key",name);

                String language = Paper.book().read("language");
                if(language.equals("th")){
                    getSupportActionBar().setTitle(name);
                }else{
                    getSupportActionBar().setTitle(place);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        txtLot = (TextView) findViewById(R.id.txtLot);
        txtStart = (TextView) findViewById(R.id.txtStart);
        txtEnd = (TextView) findViewById(R.id.txtEnd);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtPlace = (TextView) findViewById(R.id.txtPlace);
        txtZone = (TextView) findViewById(R.id.txtZone);

        Button btnOK = (Button) findViewById(R.id.btnOK);



        if(statusBook == true){
            btnOK.setText(R.string.cancel);
            DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
            DatabaseReference mDataUser = mRootRef.child(place+"/"+zone+"/"+lot);

            mDataUser.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                    Lot c = dataSnapshot.getValue(Lot.class);

                    txtPlace.setText(place);
                    txtZone.setText(zone);
                    txtLot.setText(lot);
                    txtStart.setText(c.getStartDateTime());
                    txtEnd.setText(c.getEndDateTime());




                    final long diff = checkDateTimeStartAndEnd(c.getStartDateTime() , c.getEndDateTime());

                    txtPrice.setText(diff * price + " บาท");
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        datetimeStart();
        datetimeEnd();

        txtPlace.setText(place);
        txtZone.setText(zone);
        txtLot.setText(lot);
        txtPrice.setText(price+" บาท");

        txtStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dateTimeFragmentStart.startAtCalendarView();
                dateTimeFragmentStart.setDefaultDateTime(new Date());
                dateTimeFragmentStart.setDefaultMinute(0);

                dateTimeFragmentStart.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT_STRAT);


            }
        });


        txtEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTimeFragmentEnd.startAtCalendarView();

                dateTimeFragmentEnd.setDefaultMinute(0);
                dateTimeFragmentEnd.setDefaultDateTime(new Date());
                dateTimeFragmentEnd.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT_END);

            }
        });



        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(statusBook == false ) {

                    final long diff = checkDateTimeStartAndEnd(datetimeStart, datetimeEnd);

                    if (diff > 0) {


                        AlertDialog.Builder builder = new AlertDialog.Builder(AgreeToBookActivity.this);
                        builder.setMessage("The price is "+diff*price+" baht. Do you want to book?")
                                .setCancelable(true)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Map<String, Object> map = new HashMap<>();
                                        map.put("ReserveStatus", 1);
                                        map.put("uid", FirebaseAuth.getInstance().getCurrentUser().getUid());
                                        map.put("startDateTime", txtStart.getText());
                                        map.put("endDateTime", txtEnd.getText());
                                        map.put("priceStatus", 0);

                                        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                                        Task<Void> mDataUser = mRootRef.child(place + "/" + zone + "/" + lot).updateChildren(map);

                                        mDataUser.addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                                if (task.isSuccessful()) {

                                                    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                                                    String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                                    String key = database.child("data_user/" + uid + "/history").push().getKey();

                                                    String priceShow = (diff * price) + "";
                                                    History history = new History(place, zone, lot, txtStart.getText().toString(), txtEnd.getText().toString(), 0, price, priceShow);

                                                    database.child("data_user/" + uid + "/history").child(key).setValue(history);


                                                    Map<String, Object> map = new HashMap<>();
                                                    map.put("idRefHis", key);
                                                    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                                                    Task<Void> mDataUser = mRootRef.child(place + "/" + zone + "/" + lot).updateChildren(map);


                                                    Toast.makeText(getApplicationContext(), "Book Success", Toast.LENGTH_LONG).show();

//                                                    finish();

                                                    startActivity(new Intent(AgreeToBookActivity.this , ControlPanelActivity.class));

                                                } else {
                                                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();

                    }

            } else{
                    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                    mRootRef.child(place+"/"+zone+"/"+lot+"/startDateTime").removeValue();
                    mRootRef.child(place+"/"+zone+"/"+lot+"/endDateTime").removeValue();
                    mRootRef.child(place+"/"+zone+"/"+lot+"/priceStatus").removeValue();
                    mRootRef.child(place+"/"+zone+"/"+lot+"/uid").removeValue();

                    Map<String , Object> map = new HashMap<>();
                    map.put("ReserveStatus" , 0);
                    Task<Void> mDataUser = mRootRef.child(place+"/"+zone+"/"+lot).updateChildren(map);


                }

            }
        });



    }



    private void datetimeStart(){

        dateTimeFragmentStart = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_STRAT);
        if(dateTimeFragmentStart == null) {
            dateTimeFragmentStart = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        final SimpleDateFormat myDateFormat = new SimpleDateFormat("d/MM/yyyy HH:00", java.util.Locale.getDefault());

        dateTimeFragmentStart.set24HoursMode(true);
        dateTimeFragmentStart.setHighlightAMPMSelection(false);

        try {
            dateTimeFragmentStart.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("Datetime ", e.getMessage());
        }


        dateTimeFragmentStart.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {

                Log.i("myDateFormat" , myDateFormat.format(date)+"");

                datetimeStart = myDateFormat.format(date);
                txtStart.setText(myDateFormat.format(date));
            }

            @Override
            public void onNegativeButtonClick(Date date) {

            }


            @Override
            public void onNeutralButtonClick(Date date) {

                Log.i("myDateFormat" , myDateFormat.format(date)+"");

                // Optional if neutral button does'nt exists
                txtStart.setText("");
            }
        });
    }


    private void datetimeEnd(){

        dateTimeFragmentEnd = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_END);
        if(dateTimeFragmentEnd == null) {
            dateTimeFragmentEnd = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        final SimpleDateFormat myDateFormat = new SimpleDateFormat("d/MM/yyyy HH:00", java.util.Locale.getDefault());

        dateTimeFragmentEnd.set24HoursMode(true);
        dateTimeFragmentEnd.setHighlightAMPMSelection(false);

        try {
            dateTimeFragmentEnd.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("Datetime ", e.getMessage());
        }

        dateTimeFragmentEnd.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {

                datetimeEnd = myDateFormat.format(date);
                txtEnd.setText(myDateFormat.format(date));



            }

            @Override
            public void onNegativeButtonClick(Date date) {

            }


            @Override
            public void onNeutralButtonClick(Date date) {

                Log.i("myDateFormat" , myDateFormat.format(date)+"");

                // Optional if neutral button does'nt exists
                txtEnd.setText("");
            }
        });
    }


    private long checkDateTimeStartAndEnd( String dateStart , String dateStop){

        SimpleDateFormat format = new SimpleDateFormat("d/MM/yyyy HH");

        Date d1 = null;
        Date d2 = null;
        long diffHours = -1;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);


            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
            long diffDays = diff / (24 * 60 * 60 * 1000);


            Log.i("diffHours" , diffHours + "hours");
            Log.i("diffMinutes" , diffMinutes + "minutes");


        } catch (Exception e) {
            e.printStackTrace();
        }

        return diffHours;
    }
}
