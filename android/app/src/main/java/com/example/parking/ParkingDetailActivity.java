package com.example.parking;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;

public class ParkingDetailActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;


    // TODO: Rename and change types of parameters
    private String place;
    private String name;
    private double lat;
    private double lng;

    private  double price;

    private int zoneAll = 0;
    private int lotAll = 0;
    private int avalot = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_detail);



        place = getIntent().getStringExtra("place");
        lat =  getIntent().getDoubleExtra("lat"  , -1);
        lng =  getIntent().getDoubleExtra("lng" , -1);
        price =  getIntent().getDoubleExtra("price" , -1);
//        name = getIntent().getStringExtra("name");


//        getSupportActionBar().setTitle(name);

        DatabaseReference mRootRefer = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mTitleRef = mRootRefer.child(place + "/nameTH");

        mTitleRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                name = (String) dataSnapshot.getValue();
                Log.i("key",name);

                String language = Paper.book().read("language");
                if(language.equals("th")){
                    getSupportActionBar().setTitle(name);
                }else{
                    getSupportActionBar().setTitle(place);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Log.i("dd" ,place + " " + lat + " " + lng +" " +  price );


        mRecyclerView = (RecyclerView) findViewById(R.id.zoneList);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext() ));
//
//
//
        Button btnGoogleMap = (Button) findViewById(R.id.btnGoogleMap);
        btnGoogleMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://maps.google.com/maps?f=d&daddr="+lat+","+lng+"&dirflg=d&layer=t";
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });
        Log.i("onCreateView" , place);

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();


        final TextView txt_number = (TextView) findViewById(R.id.txt_number);
        final DatabaseReference dataPlace = mRootRef.child(place+"/");
        final TextView txt_Zones = (TextView) findViewById(R.id.txt_Zones);


        dataPlace.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<Map<String , Zone>> list = new ArrayList<>();

                zoneAll = 0;
                lotAll = 0;
                avalot= 0;
                for(DataSnapshot mss : dataSnapshot.getChildren()){


                    if(!(mss.getKey().equals("ATotalZone") || mss.getKey().equals("imageZone") || mss.getKey().equals("lat") || mss.getKey().equals("lng") ||
                            mss.getKey().equals("long") || mss.getKey().equals("nameEN") || mss.getKey().equals("nameTH") || mss.getKey().equals("openingTime")||
                            mss.getKey().equals("priceRange"))){

                        Log.i("mss" , mss.getKey());



                        Zone zone = mss.getValue(Zone.class);

                        zone.setPlace(place);
                        zone.setKey(mss.getKey());
                        zone.setPrice(price);
                        Log.i("zone " , zone.getAAvailable()+"");
                        Log.i("zone " , zone.getATotalLot()+"");

                        Map<String , Zone> map = new HashMap<>();
                        map.put(mss.getKey() , zone);
                        list.add(map);

                        lotAll  += zone.getATotalLot();
                        avalot += zone.getAAvailable();
                        zoneAll += 1;


                    }
                }



                if(list.size() >= 1){


                    txt_number.setText(avalot+"");
                    txt_Zones.setText(zoneAll+"");

                    mAdapter = new adapterZone(list, getApplicationContext() );
                    mRecyclerView.setAdapter(mAdapter);


                }else{
                    txt_number.setText("Null");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_parking_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.findFavorite) {


            addFavorite(place);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addFavorite(String place){



        DatabaseReference database = FirebaseDatabase.getInstance().getReference();


        String keyUser = FirebaseAuth.getInstance().getCurrentUser().getUid();

        String key = database.child("data_user/"+keyUser+"/favorite").push().getKey();



        database.child("data_user/"+keyUser+"/favorite").child(place).setValue(place);

        Toast.makeText(getApplicationContext() , "เสร็จสิ้น" , Toast.LENGTH_SHORT).show();


    }
}
