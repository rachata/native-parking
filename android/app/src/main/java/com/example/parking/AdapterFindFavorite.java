package com.example.parking;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class AdapterFindFavorite  extends RecyclerView.Adapter<AdapterFindFavorite.ViewHolder> {



    private List<String> data;

    private Context context;


    public AdapterFindFavorite(List<String> data , Context context ){

        this.data = data;

        this.context = context;

    }


    @NonNull
    @Override
    public AdapterFindFavorite.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_find, viewGroup, false);

        return new AdapterFindFavorite.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFindFavorite.ViewHolder viewHolder, int i) {


        if(data != null){
            String name = data.get(i);


            viewHolder.imageFa.setTag(R.drawable.star_y);
            viewHolder.imageFa.setImageDrawable( context. getResources().getDrawable(R.drawable.star_y));
            viewHolder.name.setText(name);
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name;
        public ImageView imageFa;


        public ViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.txtName);
            imageFa = (ImageView) view.findViewById(R.id.imageFa);


            imageFa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String keyUser = FirebaseAuth.getInstance().getCurrentUser().getUid();

                    String keyPlace = data.get(getLayoutPosition());

                    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                    mRootRef.child("data_user/"+keyUser+"/favorite/"+keyPlace).removeValue();

                }
            });




            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(context , PreviewActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("data" , data.get(getLayoutPosition()));
            context.startActivity(intent);
        }
    }


}
