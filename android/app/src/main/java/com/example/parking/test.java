package com.example.parking;


import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class test {


    private String bookName;
    private String email;
    private String fName;
    private String name;
    private String phone;


    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public test(String bookName, String email, String fName, String name, String phone) {
        this.bookName = bookName;
        this.email = email;
        this.fName = fName;
        this.name = name;
        this.phone = phone;
    }


    public test(){}
}
