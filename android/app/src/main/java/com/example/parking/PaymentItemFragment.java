package com.example.parking;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import co.omise.android.models.Token;
import co.omise.android.ui.CreditCardActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentItemFragment extends Fragment {



    public PaymentItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle(R.string.payment);
        View v= inflater.inflate(R.layout.fragment_payment_item, container, false);


        Button btnCredit = (Button) v.findViewById(R.id.btnCredit);

        btnCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext() , CreditActivity.class);
                startActivity(intent);

            }
        });
        return v;
    }




}
