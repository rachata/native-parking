package com.example.parking;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class PreviewActivity extends AppCompatActivity {

    private ImageView imgZone;
    private Target mTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Log.i("onBitmapLoaded", bitmap.getHeight() + " " + bitmap.getWidth());


            imgZone.setImageBitmap(bitmap);

//            setBitmaps(bitmap);
//
//            initCanvas();


        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            Log.i("onBitmapFailed", "onBitmapFailed");
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

            Log.i("onPrepareLoad", "onPrepareLoad");
        }
    };


    private double lat;
    private double lng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        Intent intent = getIntent();
        final String data = intent.getStringExtra("data");


        imgZone = (ImageView) findViewById(R.id.imageZone);




        Button navi = (Button) findViewById(R.id.navi);
        navi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        String url = "http://maps.google.com/maps?f=d&daddr="+lat+","+lng+"&dirflg=d&layer=t";
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
            }
        });





        final TextView txtZone = (TextView) findViewById(R.id.zone);
        final TextView txtTime = (TextView) findViewById(R.id.time);
        final TextView txtPrice = (TextView) findViewById(R.id.price);



        final Button btnPlace = (Button) findViewById(R.id.openZone);

        btnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String price = txtPrice.getText().toString();
                double d=Double.parseDouble(price);

                Intent intent  =  new Intent(PreviewActivity.this, ParkingDetailActivity.class);
                intent.putExtra("place" , data);
                intent.putExtra("price" , d);
                intent.putExtra("lat" , lat);
                intent.putExtra("lng" , lng);
                startActivity(intent);

            }
        });
        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference dataPlace = mRootRef.child(data+"/");

        dataPlace.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                List<String > list = new ArrayList<>();
                for(DataSnapshot mss  : dataSnapshot.getChildren()){


                    if(mss.getKey().equals("ATotalZone")){
                        txtZone.setText(mss.getValue().toString());
                    }

                    if(mss.getKey().equals("imageZone")){

                        Picasso.get()
                                .load(mss.getValue().toString())
                                .into(mTarget);

                    }

                    if(mss.getKey().equals("openingTime")){
                        txtTime.setText(mss.getValue().toString());
                    }

                    if(mss.getKey().equals("priceRange")){
                        txtPrice.setText(mss.getValue().toString());
                    }

                    if(mss.getKey().equals("lat")){

                        lat = (double)mss.getValue();
                    }

                    if(mss.getKey().equals("lng")){

                        lng = (double)mss.getValue();
                    }


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
