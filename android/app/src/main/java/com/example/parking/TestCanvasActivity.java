package com.example.parking;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;

public class TestCanvasActivity extends AppCompatActivity {

    private LinearLayout linearLayout;
    private  String name;
    private int heightToolbar;
    private int widthScreen;
    private int heightScreen;
    private Bitmap bitmaps;
    private double price;
    private String place;
    private String zone;
    private List<Map<String, Lot>> data;

    void setBitmaps(Bitmap bitmap) {
        this.bitmaps = bitmap;
    }

    private Target mTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Log.i("onBitmapLoaded", bitmap.getHeight() + " " + bitmap.getWidth());


            setBitmaps(bitmap);

            initCanvas();
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            Log.i("onBitmapFailed", "onBitmapFailed : " + e.getMessage());
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

            Log.i("onPrepareLoad", "onPrepareLoad");
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Paper.init(this);
        setContentView(R.layout.activity_test_canvas);



//        สามารถจองได้เฉพาะช่องจอดที่มีสัญลักษณ์ R เท่านั้น สีแดงคือมีคนจองแล้ว หรือ รถจอดอยู่


        place = getIntent().getStringExtra("place");
        zone = getIntent().getStringExtra("zone");
        price = getIntent().getDoubleExtra("price", -1);




        Log.i("price " ,price+" ");
        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

        DatabaseReference mUsersRef = mRootRef.child(place + "/" + zone);

        DatabaseReference mTitleRef = mRootRef.child(place + "/nameTH");

        mTitleRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                name = (String) dataSnapshot.getValue();
                Log.i("key",name);

                String language = Paper.book().read("language");
                if(language.equals("th")){
                    getSupportActionBar().setTitle(name);
                }else{
                    getSupportActionBar().setTitle(place);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





        mUsersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String url = null;
                List<Map<String, Lot>> list = new ArrayList<>();
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {


                    if (!messageSnapshot.getKey().trim().toString().equals("AAvailable") && !messageSnapshot.getKey().trim().toString().equals("ATotalLot")
                            && !messageSnapshot.getKey().trim().toString().equals("name") && !messageSnapshot.getKey().trim().toString().equals("URL")) {

                        Log.i("messageSnapshot", messageSnapshot.getKey() + "");


                        Map<String, Lot> map = new HashMap<>();
                        map.put(messageSnapshot.getKey(), messageSnapshot.getValue(Lot.class));

                        list.add(map);
                    }

                    if (messageSnapshot.getKey().trim().toString().equals("URL")) {

                        url = messageSnapshot.getValue().toString();


                    }
                }


                if (url != null) {

                    data = list;
                    Log.i("list Data ", list.size() + " ");
                    Log.i("URL IMAGE ", url);


                    if (((LinearLayout) linearLayout).getChildCount() > 0) {
                        ((LinearLayout) linearLayout).removeAllViews();


                    }

                    Picasso.get()
                            .load(url)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .resize(1024, 2048)
                            .into(mTarget);

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        FloatingActionButton btnReset = (FloatingActionButton) findViewById(R.id.btnReset);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("FloatingActionButton", "FloatingActionButton");

                if (((LinearLayout) linearLayout).getChildCount() > 0) {
                    ((LinearLayout) linearLayout).removeAllViews();
                    initCanvas();

                }
            }
        });

    }


    private void initCanvas() {
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            heightToolbar = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            widthScreen = displayMetrics.widthPixels;
            heightScreen = displayMetrics.heightPixels;

            Log.i("Toolbar ", heightToolbar + "");

            Log.i("Screen 1 ", "height " + heightScreen + " width " + widthScreen);


            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            PinchZoomPan view = new PinchZoomPan(getApplicationContext(), heightScreen, widthScreen, heightToolbar, this.bitmaps, data, this.place , this.zone , this.price, uid);

            int canvasW = (int) view.getImageW();
            int canvasH = (int) view.getImageH();

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(canvasW, canvasH);

            linearLayout.addView(view, params);


        }
    }

    public void createAlert(String str){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());

        // set title
        alertDialogBuilder.setTitle("คำธิบาย");

        // set dialog message
        alertDialogBuilder
                .setMessage(str);


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


        Log.i("wefwef" , "wefwefwefewf");
    }
}
