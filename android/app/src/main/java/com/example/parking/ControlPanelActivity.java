package com.example.parking;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.example.parking.Helper.LocaleHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

import io.paperdb.Paper;

public class ControlPanelActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Paper.init(this);



        setContentView(R.layout.activity_control_panel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        super.onCreate(savedInstanceState);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNav);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        View headerView = navigationView.getHeaderView(0);

        final TextView txtName = (TextView)   headerView.findViewById(R.id.txtName);
        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

        String uid= FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference displayname = mRootRef.child("data_user/"+uid+"/displayname");
        displayname.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                txtName.setText(dataSnapshot.getValue()+"");

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        Button profile = (Button) headerView.findViewById(R.id.goProfile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                ProfileFragment profileFragment = new ProfileFragment();
                replaceFragmentManager(profileFragment);



            }
        });

        Button logout = (Button) headerView.findViewById(R.id.goLogout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);


                FirebaseAuth auth;
                auth = FirebaseAuth.getInstance();
                if(auth.getCurrentUser() != null){
                    auth.signOut();
                    Intent intent = new Intent(ControlPanelActivity.this , LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });


        String language = Paper.book().read("language");
        String getLang = Locale.getDefault().getLanguage();

        HomeFragment homeFragment = new HomeFragment();
        replaceFragmentManager(homeFragment);

        if(language == null) {
            Paper.book().write("language", "en");
        }else{
            if(language.equals(getLang)){
                updateView((String)Paper.book().read("language"));
            }else{
                updateView((String)Paper.book().read("language"));
            }
        }

        updateView((String)Paper.book().read("language"));



    }


    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.bottomFavorite) {
                Log.i("aaa", "Favorite");
                Intent intent = new Intent(ControlPanelActivity.this, FindFavoriteActivity.class);
                startActivity(intent);
            } else if (id == R.id.bottomHome) {
                Log.i("aaa", "Home");
                HomeFragment homeFragment = new HomeFragment();
                replaceFragmentManager(homeFragment);
            } else if (id == R.id.bottomNews) {
                Log.i("aaa", "News");
                NewsFragment newsFragment = new NewsFragment();
                replaceFragmentManager(newsFragment);
            } else if (id == R.id.bottomParking) {
                Intent intent = new Intent(ControlPanelActivity.this, ViewMapsActivity.class);
                startActivity(intent);
            }


            return true;
        }
    };


    private  void  updateView(String lang){
        Context context = LocaleHelper.setLocale(this,lang);
        context.getResources();
//        Resources resources = context.getResources();
//        textView.setText(resources.getString(R.string.hello));

        Configuration config = new Configuration();
        config.locale = new Locale(lang);
        getResources().updateConfiguration(config, null);

    }

    @Override
    protected void onResume() {

        String language = Paper.book().read("language");
        String getLang = Locale.getDefault().getLanguage();

        HomeFragment homeFragment = new HomeFragment();
        replaceFragmentManager(homeFragment);

        if(language == null) {
            Paper.book().write("language", "en");
        }else{
            if(language.equals(getLang)){
                updateView((String)Paper.book().read("language"));
            }else{
                updateView((String)Paper.book().read("language"));
            }
        }

        updateView((String)Paper.book().read("language"));

        super.onResume();

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.control_panel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.findFavorite) {
            startActivity(new Intent(ControlPanelActivity.this , FindFavoriteActivity.class));

            return true;
        }else if(id == R.id.findALL){
            startActivity(new Intent(ControlPanelActivity.this , FindAllActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            HomeFragment homeFragment = new HomeFragment();
            replaceFragmentManager(homeFragment);
        } else if (id == R.id.nav_profile) {
            ProfileFragment profileFragment = new ProfileFragment();
            replaceFragmentManager(profileFragment);
        } else if (id == R.id.nav_map) {
           startActivity(new Intent(ControlPanelActivity.this, ViewMapsActivity.class));
        } else if (id == R.id.nav_contact) {
            ContactFragment contactUsFragment = new ContactFragment();
            replaceFragmentManager(contactUsFragment);
        }else if(id == R.id.nav_thai){
            Paper.book().write("language","th");
            updateView((String)Paper.book().read("language"));
            finish();
            startActivity(getIntent());
        }else if(id == R.id.nav_eng){
            Paper.book().write("language","en");
            updateView((String)Paper.book().read("language"));
            finish();
            startActivity(getIntent());
        }else if(id == R.id.payment){
            PaymentItemFragment paymentItemFragment = new PaymentItemFragment();
            replaceFragmentManager(paymentItemFragment);
        }else if(id == R.id.park){
            Intent intent = new Intent(this , ParkActivity.class);
            startActivity(intent);
        }else if(id == R.id.logout){
            FirebaseAuth auth;
            auth = FirebaseAuth.getInstance();
            if(auth.getCurrentUser() != null){
                auth.signOut();
                Intent intent = new Intent(this , LoginActivity.class);
                startActivity(intent);
                finish();
            }

        } else if (id == R.id.news) {

            NewsFragment newsFragment = new NewsFragment();
            replaceFragmentManager(newsFragment);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private void replaceFragmentManager(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }
}
