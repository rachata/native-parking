package com.example.parking;

import com.google.firebase.database.PropertyName;

public class News {

    @PropertyName("titleTH") private String titleTH;
    @PropertyName("titleEN") private  String titleEN;

    @PropertyName("contentTH") private String contentTH;
    @PropertyName("contentEN") private String contentEN;

    @PropertyName("date") private String date;

    @PropertyName("url") private String url;



    public  News(){

    }


    public News(String titleTH, String titleEN, String contentTH, String contentEN, String date, String url) {
        this.titleTH = titleTH;
        this.titleEN = titleEN;
        this.contentTH = contentTH;
        this.contentEN = contentEN;
        this.date = date;
        this.url = url;
    }

    public String getTitleTH() {
        return titleTH;
    }

    public void setTitleTH(String titleTH) {
        this.titleTH = titleTH;
    }

    public String getTitleEN() {
        return titleEN;
    }

    public void setTitleEN(String titleEN) {
        this.titleEN = titleEN;
    }

    public String getContentTH() {
        return contentTH;
    }

    public void setContentTH(String contentTH) {
        this.contentTH = contentTH;
    }

    public String getContentEN() {
        return contentEN;
    }

    public void setContentEN(String contentEN) {
        this.contentEN = contentEN;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
