package com.example.parking;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;


public class MyView  extends View {


    Paint paint;
    Path path;
    private Context context;
    private int height;
    private int width;
    private int toolbar;

    private boolean zoomenable=false;
    private ArrayList<Bitmap> bmps=new ArrayList<Bitmap>();
    private static float MIN_ZOOM=1f;
    private static float MAX_ZOOM=5f;
    private float scaleFactor=1.f;
    private ScaleGestureDetector detector;
    private static int NONE=0;
    private static int DRAG=1;
    private static int ZOOM=2;
    private int mode;
    private float startX=0f;
    private float startY=0f;
    private float translateX=0f;
    private float translateY=0f;
    private float previusTranslateX=0f;
    private float previusTranslateY=0f;
    private boolean dragged=false;
    private float displayWidth;
    private float displayHeight;


    public MyView(Context context , int height , int width , int toolbar) {

        super(context);
        context = context;
        this.height = height;
        this.width  = width;
        this.toolbar = toolbar;

        detector=new ScaleGestureDetector(getContext(), new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                return false;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return false;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {

            }
        });

        WindowManager wm=(WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display=wm.getDefaultDisplay();
        displayHeight=display.getHeight();
        displayWidth=display.getWidth();

        init();
    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        context = context;
        init();
    }

    public MyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        context = context;
        init();
    }

    private void init(){
        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(10);
        paint.setStyle(Paint.Style.STROKE);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);




        int ResultHegiht = height - toolbar;

        Log.i("Screen " , "H " + height + " W " + width + " toolbar " + ResultHegiht);
        canvas.drawRect(0, 0, 300, 300, paint);

        if(zoomenable==true) {

            canvas.save();

//            if(bmps.size()==1){
//                bmps.add(canvasBitmap);
//            }
//            canvas.drawBitmap(bmps.get(bmps.size() - 1), 0, 0, canvasPaint);
//            canvas.drawPath(drawPath, drawPaint);

            canvas.scale(scaleFactor, scaleFactor);
            if (translateX * -1 < 0) {
                translateX = 0;
            } else if ((translateX * -1) > (scaleFactor - 1) * displayWidth) {
                translateX = (1 - scaleFactor) * displayWidth;
            }

            if (translateY * -1 < 0) {
                translateY = 0;
            } else if ((translateY * -1) > (scaleFactor - 1) * displayHeight) {
                translateY = (1 - scaleFactor) * displayHeight;
            }
            canvas.translate(translateX / scaleFactor, translateY / scaleFactor);
            canvas.restore();
        }else {

            //Draw shapes preview

        }
    }

    public boolean onTouchEvent(MotionEvent event){

        if(zoomenable==false) {

            //touch events for drawing

        }
        if(zoomenable==true){
            switch (event.getAction()&MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    mode=DRAG;
                    startX=event.getX()-previusTranslateX;
                    startY=event.getY()-previusTranslateY;
                    break;

                case MotionEvent.ACTION_MOVE:
                    translateX=event.getX()-startX;
                    translateY=event.getY()-startY;
                    double distance=Math.sqrt(Math.pow(event.getX()-(startX+previusTranslateX),2)+Math.pow(event.getY()-(startY+previusTranslateY),2));
                    if(distance>0){
                        dragged=true;
                    }
                    break;

                case MotionEvent.ACTION_POINTER_DOWN:
                    mode=ZOOM;
                    break;

                case MotionEvent.ACTION_UP:
                    mode=NONE;
                    dragged=false;
                    previusTranslateY=translateY;
                    previusTranslateX=translateX;
                    break;

                case MotionEvent.ACTION_POINTER_UP:
                    mode=DRAG;
                    previusTranslateX=translateX;
                    previusTranslateY=translateY;
                    break;
            }
            detector.onTouchEvent(event);
            if((mode==DRAG&&scaleFactor!=1f&&dragged)||mode==ZOOM){
                invalidate();
            }
        }
        return true;
    }

}
