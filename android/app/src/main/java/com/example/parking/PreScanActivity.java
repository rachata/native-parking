package com.example.parking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.luseen.simplepermission.permissions.Permission;
import com.luseen.simplepermission.permissions.PermissionActivity;
import com.luseen.simplepermission.permissions.PermissionUtils;
import com.luseen.simplepermission.permissions.SinglePermissionCallback;

public class PreScanActivity extends PermissionActivity {


    private static final int REQUEST_MAIN = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_scan);

        Button btn = (Button) findViewById(R.id.btn);
        setTitle(R.string.park);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(PermissionUtils.isMarshmallowOrHigher()){

                    requestPermission(Permission.CAMERA, new SinglePermissionCallback() {
                        @Override
                        public void onPermissionResult(boolean permissionGranted, boolean isPermissionDeniedForever) {
                            if(permissionGranted){
                                Intent intent = new Intent(getApplicationContext(), ScanBarcodeActivity.class);
                                startActivityForResult(intent , REQUEST_MAIN);
                            }else{

                            }
                        }
                    });
                }else{
                    Intent intent = new Intent(getApplicationContext(), ScanBarcodeActivity.class);
                    startActivityForResult(intent , REQUEST_MAIN);
                }


            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_MAIN) {
            if (resultCode == RESULT_OK){
                String dataQRCode  = data.getStringExtra("data");

                Log.i("dataQRCode" , dataQRCode);


            }else if(resultCode == RESULT_CANCELED){
                //TODO Handle Result Cancel
            }

        }
    }


}
