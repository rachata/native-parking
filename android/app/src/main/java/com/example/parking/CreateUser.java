package com.example.parking;



import com.google.firebase.database.PropertyName;


public class CreateUser {

    @PropertyName("fullname") private String fullname;
    @PropertyName("displayname") private String displayname;
    @PropertyName("telephone") private String telephone;
    @PropertyName("carbrand") private String carbrand;
    @PropertyName("carmodel") private String carmodel;
    @PropertyName("dateRegis") private String dateRegis;
    @PropertyName("lastLogin") private String lastLogin;


    public CreateUser(){ }


    public CreateUser(String fullname, String displayname, String telephone, String carbrand, String carmodel, String dateRegis, String lastLogin) {
        this.fullname = fullname;
        this.displayname = displayname;
        this.telephone = telephone;
        this.carbrand = carbrand;
        this.carmodel = carmodel;
        this.dateRegis = dateRegis;
        this.lastLogin = lastLogin;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCarbrand() {
        return carbrand;
    }

    public void setCarbrand(String carbrand) {
        this.carbrand = carbrand;
    }

    public String getCarmodel() {
        return carmodel;
    }

    public void setCarmodel(String carmodel) {
        this.carmodel = carmodel;
    }

    public String getDateRegis() {
        return dateRegis;
    }

    public void setDateRegis(String dateRegis) {
        this.dateRegis = dateRegis;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }
}
