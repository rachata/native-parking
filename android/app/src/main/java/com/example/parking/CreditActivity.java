package com.example.parking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import co.omise.android.models.Token;
import co.omise.android.ui.CreditCardActivity;

public class CreditActivity extends AppCompatActivity {


    private static final String OMISE_PKEY = "pkey_test_5gar8nyesyu78aots9d";
    private static final int REQUEST_CC = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit);

        showCreditCardForm();
    }

    private void showCreditCardForm() {
        Intent intent = new Intent(this, CreditCardActivity.class);
        intent.putExtra(CreditCardActivity.EXTRA_PKEY, OMISE_PKEY);
        startActivityForResult(intent, REQUEST_CC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CC:
                if (resultCode == RESULT_CANCELED) {
                    return;
                }

                Token token = data.getParcelableExtra(CreditCardActivity.EXTRA_TOKEN_OBJECT);
                // process your token here.



                Log.i("resultCode" , resultCode+"");
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
