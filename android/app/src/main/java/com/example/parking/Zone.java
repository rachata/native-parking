package com.example.parking;

        import com.google.firebase.database.PropertyName;

public class Zone {

    @PropertyName("AAvailable") private int AAvailable;
    @PropertyName("ATotalLot") private int ATotalLot;
    @PropertyName("name") private String name;

    private String key;
    private String place;
    private  double price;


    public Zone(){}
    public Zone(int AAvailable, int ATotalLot, String name, String key, String place, double price) {
        this.AAvailable = AAvailable;
        this.ATotalLot = ATotalLot;
        this.name = name;
        this.key = key;
        this.place = place;
        this.price = price;
    }

    public int getAAvailable() {
        return AAvailable;
    }

    public void setAAvailable(int AAvailable) {
        this.AAvailable = AAvailable;
    }

    public int getATotalLot() {
        return ATotalLot;
    }

    public void setATotalLot(int ATotalLot) {
        this.ATotalLot = ATotalLot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
