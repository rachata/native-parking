package com.example.parking;

import com.google.firebase.database.PropertyName;

public class Lot {

    @PropertyName("AccessX1") private double  accessX1;



    @PropertyName("AccessX2") private double accessX2;
    @PropertyName("AccessY1") private double accessY1;
    @PropertyName("AccessY2") private double accessY2;
    @PropertyName("Battery") private double battery;
    @PropertyName("CHH") private double chh;
    @PropertyName("CHW") private double chw;
    @PropertyName("GAP") private double gap;
    @PropertyName("ParkStatus") private int parkStatus;
    @PropertyName("PosX1") private double posX1;
    @PropertyName("PosY1") private double posY1;
    @PropertyName("ReserveStatus") private int reserveStatus;
    @PropertyName("Reserveable") private int reserveAble;
    @PropertyName("SensorData") private double sensorData;
    @PropertyName("Threshold") private double threshold;
    @PropertyName("angle") private double angle;

    @PropertyName("endDateTime") private String endDateTime;
    @PropertyName("startDateTime") private String startDateTime;
    @PropertyName("idRefHis") private String idRefHis;
    @PropertyName("priceStatus") private int priceStatus;
    @PropertyName("uid") private String uid;


    public Lot(){}


    public Lot(double accessX1, double accessX2, double accessY1, double accessY2, double battery, double chh, double chw, double gap, int parkStatus, double posX1, double posY1, int reserveStatus, int reserveAble, double sensorData, double threshold, double angle, String endDateTime, String startDateTime, String idRefHis, int priceStatus, String uid) {
        this.accessX1 = accessX1;
        this.accessX2 = accessX2;
        this.accessY1 = accessY1;
        this.accessY2 = accessY2;
        this.battery = battery;
        this.chh = chh;
        this.chw = chw;
        this.gap = gap;
        this.parkStatus = parkStatus;
        this.posX1 = posX1;
        this.posY1 = posY1;
        this.reserveStatus = reserveStatus;
        this.reserveAble = reserveAble;
        this.sensorData = sensorData;
        this.threshold = threshold;
        this.angle = angle;
        this.endDateTime = endDateTime;
        this.startDateTime = startDateTime;
        this.idRefHis = idRefHis;
        this.priceStatus = priceStatus;
        this.uid = uid;
    }

    public double getAccessX1() {
        return accessX1;
    }

    public void setAccessX1(double accessX1) {
        this.accessX1 = accessX1;
    }

    public double getAccessX2() {
        return accessX2;
    }

    public void setAccessX2(double accessX2) {
        this.accessX2 = accessX2;
    }

    public double getAccessY1() {
        return accessY1;
    }

    public void setAccessY1(double accessY1) {
        this.accessY1 = accessY1;
    }

    public double getAccessY2() {
        return accessY2;
    }

    public void setAccessY2(double accessY2) {
        this.accessY2 = accessY2;
    }

    public double getBattery() {
        return battery;
    }

    public void setBattery(double battery) {
        this.battery = battery;
    }

    public double getChh() {
        return chh;
    }

    public void setChh(double chh) {
        this.chh = chh;
    }

    public double getChw() {
        return chw;
    }

    public void setChw(double chw) {
        this.chw = chw;
    }

    public double getGap() {
        return gap;
    }

    public void setGap(double gap) {
        this.gap = gap;
    }

    public int getParkStatus() {
        return parkStatus;
    }

    public void setParkStatus(int parkStatus) {
        this.parkStatus = parkStatus;
    }

    public double getPosX1() {
        return posX1;
    }

    public void setPosX1(double posX1) {
        this.posX1 = posX1;
    }

    public double getPosY1() {
        return posY1;
    }

    public void setPosY1(double posY1) {
        this.posY1 = posY1;
    }

    public int getReserveStatus() {
        return reserveStatus;
    }

    public void setReserveStatus(int reserveStatus) {
        this.reserveStatus = reserveStatus;
    }

    public int getReserveAble() {
        return reserveAble;
    }

    public void setReserveAble(int reserveAble) {
        this.reserveAble = reserveAble;
    }

    public double getSensorData() {
        return sensorData;
    }

    public void setSensorData(double sensorData) {
        this.sensorData = sensorData;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getIdRefHis() {
        return idRefHis;
    }

    public void setIdRefHis(String idRefHis) {
        this.idRefHis = idRefHis;
    }

    public int getPriceStatus() {
        return priceStatus;
    }

    public void setPriceStatus(int priceStatus) {
        this.priceStatus = priceStatus;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
