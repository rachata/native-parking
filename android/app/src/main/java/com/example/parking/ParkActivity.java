package com.example.parking;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.luseen.simplepermission.permissions.Permission;
import com.luseen.simplepermission.permissions.PermissionActivity;
import com.luseen.simplepermission.permissions.PermissionUtils;
import com.luseen.simplepermission.permissions.SinglePermissionCallback;

import java.util.ArrayList;

public class ParkActivity extends PermissionActivity {





    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_park);

        setTitle(R.string.park);

        mRecyclerView = (RecyclerView) findViewById(R.id.parkList);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference history = mRootRef.child("data_user/"+uid+"/history");


        history.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<History> list = new ArrayList<>();

                for(DataSnapshot messageSnapshot : dataSnapshot.getChildren()){

                    History historyList = messageSnapshot.getValue(History.class);

                    list.add(historyList);



                }

                if(list.size() >= 1){
                    Log.i("list.size()" , list.size()+"");
                    mAdapter = new AdapterPark(list, getApplicationContext());
                    mRecyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




    }








}
