package com.example.parking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;

public class SplashActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);

                    if (mAuth.getCurrentUser() != null) {
                        Log.i("Uid User ", mAuth.getCurrentUser().getUid());
                        startActivity(new Intent(getApplicationContext(), SelectAppActivity.class));
                    }else{
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    }
                    finish();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}
