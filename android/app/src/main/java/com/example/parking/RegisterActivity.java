package com.example.parking;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.parking.Helper.LocaleHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

import io.paperdb.Paper;

public class RegisterActivity extends AppCompatActivity {


    private FirebaseAuth auth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Paper.init(this);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();



        Button btnRegis = (Button) findViewById(R.id.btnRegis);
        final EditText txtUsername = (EditText) findViewById(R.id.txtUsername);
        final EditText txtPassword = (EditText) findViewById(R.id.txtPassword);
        final EditText txtFullName = (EditText) findViewById(R.id.txtFullName);
        final EditText txtDisplay =  (EditText) findViewById(R.id.txtDisplayName);
        final EditText txtTelephone = (EditText) findViewById(R.id.txtPhoneNumber);
        final EditText txtCarBrand = (EditText) findViewById(R.id.txtCarBrand);
        final EditText txtCarModel = (EditText) findViewById(R.id.txtCarModel) ;
        final EditText txtRepassword = (EditText) findViewById(R.id.txtRePassword);
        final Spinner txtSpinner = (Spinner) findViewById(R.id.spinBrand);

        txtCarBrand.setEnabled(false);



        CheckBox checkBox = (CheckBox) findViewById(R.id.checkedBox);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean checked = ((CheckBox) view).isChecked();
                Log.i("aaaa", String.valueOf(checked));
                final EditText txtCarBrand = (EditText) findViewById(R.id.txtCarBrand);
                final Spinner txtSpinner = (Spinner) findViewById(R.id.spinBrand);
                if(checked == true){

                        txtCarBrand.setEnabled(true);
                        txtSpinner.setEnabled(false);
                        Log.i("aaaa", "ggggggg");

                }else if(checked == false){
                    txtCarBrand.setEnabled(false);
                    txtSpinner.setEnabled(true);
                    Log.i("aaaa", "aaaa");
                }
            }
        });


        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtPassword.getText().toString().equals(txtRepassword.getText().toString()) && txtPassword.getText().toString().length() >= 8) {

                    auth.createUserWithEmailAndPassword(txtUsername.getText().toString(), txtPassword.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (!task.isSuccessful()) {
                                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                    } else {

                                        String uId = task.getResult().getUser().getUid();

                                        Log.i("asasas", uId);

                                        boolean isChecked = ((CheckBox) findViewById(R.id.checkedBox)).isChecked();
                                        Log.i("fdfdf", String.valueOf(isChecked));
                                        String text = null;
                                        if (isChecked == false) {
                                            text = txtSpinner.getSelectedItem().toString();
                                        } else if (isChecked == true) {
                                            text = txtCarBrand.getText().toString();
                                        }

                                        Log.i("dfdgg", text);


                                        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy  HH:mm:ss");
                                        Date date = new Date(System.currentTimeMillis());




                                        writeNewUser(uId, txtFullName.getText().toString(), txtDisplay.getText().toString(), txtTelephone.getText().toString(), text, txtCarModel.getText().toString() , formatter.format(date) , formatter.format(date));

                                        Toast.makeText(getApplicationContext(), "Register Success", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }else{
                    Toast.makeText(getApplicationContext(),"Please Re-Check Password",Toast.LENGTH_LONG).show();
                }
            }
        });

        String language = Paper.book().read("language");
        String getLang = Locale.getDefault().getLanguage();



        if(language == null) {
            Paper.book().write("language", "en");
        }else{
            if(language.equals(getLang)){
                updateView((String)Paper.book().read("language"));
            }else{
                updateView((String)Paper.book().read("language"));
            }
        }

        updateView((String)Paper.book().read("language"));
    }



    private void writeNewUser(String uId ,String fullName ,String displayName ,String telephone ,String carBrand ,String carModel , String regis  , String login){
        CreateUser createUser = new CreateUser(fullName,displayName,telephone,carBrand,carModel , regis , login);

        mDatabase.child("data_user").child(uId).setValue(createUser);

    }

    private  void  updateView(String lang){
        Context context = LocaleHelper.setLocale(this,lang);
        context.getResources();
//        Resources resources = context.getResources();
//        textView.setText(resources.getString(R.string.hello));

        Configuration config = new Configuration();
        config.locale = new Locale(lang);
        getResources().updateConfiguration(config, null);

    }
}
