package com.example.parking;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment {


    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle(R.string.nav_contact);
        View v = inflater.inflate(R.layout.fragment_contact, container, false);

        final EditText txtName = (EditText) v.findViewById(R.id.txtName);
        final EditText txtTopic = (EditText) v.findViewById(R.id.txtTopic);
        final EditText txtDetail = (EditText) v.findViewById(R.id.txtDetail);
        final EditText txtTel = (EditText) v.findViewById(R.id.txtTel);
        final EditText txtEmail = (EditText) v.findViewById(R.id.txtEmail);


        Button btnSubmit = (Button) v.findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtName.getText().toString();
                String detail = txtDetail.getText().toString();
                String topic = txtTopic.getText().toString();
                String tel = txtTel.getText().toString();
                String email = txtEmail.getText().toString();


                String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                DatabaseReference database = FirebaseDatabase.getInstance().getReference();


                String key = database.child("data/contact").push().getKey();

                Contact contact = new Contact(name , detail , topic , tel , email , uid);

                database.child("data/contact").child(key).setValue(contact);


            }
        });
        return v;
    }

}
