package com.example.parking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AdapterNews  extends RecyclerView.Adapter<AdapterNews.ViewHolder> {


    private List<News> data;
    private Context context;
    public AdapterNews(List<News> data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        News news = data.get(i);


        viewHolder.title.setText(news.getTitleTH());
        viewHolder.content.setText(news.getContentTH());
        viewHolder.date.setText(news.getDate());

        viewHolder.title.setTextColor(Color.parseColor("#FFFFFF"));
        viewHolder.content.setTextColor(Color.parseColor("#FFFFFF"));
        viewHolder.date.setTextColor(Color.parseColor("#FFFFFF"));
    }

    @Override
    public int getItemCount() {
        return data.size();

    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title;
        public TextView content;
        public TextView date;

        public ViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.txtTitle);
            content = (TextView) view.findViewById(R.id.txtContent);
            date = (TextView) view.findViewById(R.id.txtDate);


            content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    
                    String url = data.get(getLayoutPosition()).getUrl();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                }
            });
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {



        }
    }
}
