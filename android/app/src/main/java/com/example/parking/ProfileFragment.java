package com.example.parking;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("fragMent","hello");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        getActivity().setTitle(R.string.profile);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment\\
        View v = inflater.inflate(R.layout.fragment_profile, container, false);


        final Spinner spinner = (Spinner) v.findViewById(R.id.editSpinBrand);
        final EditText txtName = (EditText) v.findViewById(R.id.txtName);
        final EditText txtShowName = (EditText) v.findViewById(R.id.txtShowName);
        final EditText txtPhone = (EditText) v.findViewById(R.id.txtPhone);
        final EditText txtBrand = (EditText) v.findViewById(R.id.editCarBrand);
        final EditText txtGen = (EditText) v.findViewById(R.id.txtGen);

        Button btn = (Button) v.findViewById(R.id.btnSave);

        txtBrand.setEnabled(false);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View view = inflater.inflate(R.layout.fragment_profile,container,false);
                boolean isChecked = ((CheckBox) view.findViewById(R.id.editCheckedBox)).isChecked();
                Log.i("fdfdf", String.valueOf(isChecked));
                String text = null;
                if (isChecked == false) {
                    text = spinner.getSelectedItem().toString();
                } else if (isChecked == true) {
                    text = txtBrand.getText().toString();
                }

                Log.i("dfdgg", text);

                Map<String , Object> map = new HashMap<>();
                map.put("carbrand" , text);
                map.put("carmodel" , txtGen.getText().toString());
                map.put("displayname" , txtShowName.getText().toString());
                map.put("fullname" , txtName.getText().toString());
                map.put("telephone" , txtPhone.getText().toString());


                String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                Task<Void> mDataUser = mRootRef.child("data_user/"+uid).updateChildren(map);

                mDataUser.addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if(task.isSuccessful()){
                            Toast.makeText(getContext() , "Update Success" , Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getContext() , task.getException().getMessage() , Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        CheckBox checkBox = v.findViewById(R.id.editCheckedBox);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean status = ((CheckBox) view).isChecked();

                Log.i("vvvv", String.valueOf(status));
                if(status == true){
                    txtBrand.setEnabled(true);
                    spinner.setEnabled(false);
                }else if(status == false){
                    txtBrand.setEnabled(false);
                    spinner.setEnabled(true);
                }
            }
        });

        if(FirebaseAuth.getInstance().getCurrentUser() != null){

            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();


            DatabaseReference mDataUser = mRootRef.child("data_user/"+uid);

            mDataUser.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                    CreateUser c = dataSnapshot.getValue(CreateUser.class);

                    txtName.setText(c.getFullname());
                    txtShowName.setText(c.getDisplayname());
                    txtPhone.setText(c.getTelephone());
                    txtBrand.setText(c.getCarbrand());
                    txtGen.setText(c.getCarmodel());
                    Log.i("CCC" , c.getCarbrand());


                    spinner.setSelection(((ArrayAdapter<String>)spinner.getAdapter()).getPosition(c.getCarbrand()));

                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        }
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
