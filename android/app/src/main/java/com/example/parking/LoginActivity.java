package com.example.parking;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parking.Helper.LocaleHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.paperdb.Paper;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Paper.init(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        TextView txtRegister = (TextView) findViewById(R.id.txtRegis);
        TextView txtForgot = (TextView) findViewById(R.id.txtforgot);
        final EditText txtUsername = (EditText) findViewById(R.id.txtUsername);
        final EditText txtPassword = (EditText) findViewById(R.id.txtPassword);


        if (mAuth.getCurrentUser() != null) {
            Log.i("Uid User ", mAuth.getCurrentUser().getUid());
            startActivity(new Intent(getApplicationContext(), SelectAppActivity.class));
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mAuth.signInWithEmailAndPassword(txtUsername.getText().toString(), txtPassword.getText().toString())
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    Log.i("signInWithEmail", task.getException().getMessage());
                                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                } else {

                                    if (mAuth.getCurrentUser() != null) {


                                        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy  HH:mm:ss");
                                        Date date = new Date(System.currentTimeMillis());

                                        Map<String , Object> map = new HashMap<>();
                                        map.put("lastLogin" , formatter.format(date));



                                        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                                        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                                        Task<Void> mDataUser = mRootRef.child("data_user/"+uid).updateChildren(map);

                                        mDataUser.addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                                if(task.isSuccessful()){
                                                    Toast.makeText(getApplicationContext(), "Update Success" , Toast.LENGTH_LONG).show();
                                                }else{
                                                    Toast.makeText(getApplicationContext() , task.getException().getMessage() , Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });

                                        Log.i("Uid User ", mAuth.getCurrentUser().getUid());
                                        startActivity(new Intent(getApplicationContext(), SelectAppActivity.class));
                                        finish();
                                    }

                                }


                            }


                        });
            }
        });


        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Reset Password");


                final EditText input = new EditText(LoginActivity.this);

                builder.setView(input);


                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mAuth.sendPasswordResetEmail(input.getText().toString())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                           Toast.makeText(getApplicationContext() , "Email Send" , Toast.LENGTH_LONG).show();
                                        }else {
                                            Toast.makeText(getApplicationContext() , task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });

        String language = Paper.book().read("language");
        String getLang = Locale.getDefault().getLanguage();



        if(language == null) {
            Paper.book().write("language", "en");
        }else{
            if(language.equals(getLang)){
                updateView((String)Paper.book().read("language"));
            }else{
                updateView((String)Paper.book().read("language"));
            }
        }

        updateView((String)Paper.book().read("language"));

    }

    private  void  updateView(String lang){
        Context context = LocaleHelper.setLocale(this,lang);
        context.getResources();
//        Resources resources = context.getResources();
//        textView.setText(resources.getString(R.string.hello));

        Configuration config = new Configuration();
        config.locale = new Locale(lang);
        getResources().updateConfiguration(config, null);

    }
}
